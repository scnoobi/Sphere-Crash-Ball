﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AiDifficulty",fileName = "EasyDifficultyLevel")]
public class DifficultyLevel : ScriptableObject
{
    public TextAsset levelOfDifficulty;
    
}
