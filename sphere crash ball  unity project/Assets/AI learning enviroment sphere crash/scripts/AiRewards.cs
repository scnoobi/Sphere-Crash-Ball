﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiRewards : MonoBehaviour
{
    private CrashBallAgent aiAgent;
    private void OnCollisionEnter(Collision collision)
    {


        if (collision.collider.CompareTag("Ball"))
            AddAAgentReward(1);

    }

    public void AddAAgentReward(float amount)
    {
        if (!aiAgent)
            aiAgent = GetComponent<CrashBallAgent>();

        aiAgent.AddReward(amount);
    }
    public void Finished()
    {
        if (!aiAgent)
            aiAgent = GetComponent<CrashBallAgent>();

        aiAgent.Done();
    }
}
