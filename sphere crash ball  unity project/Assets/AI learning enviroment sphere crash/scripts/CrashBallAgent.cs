﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashBallAgent : Agent
{

    private RayPerception rayPer;
    private Movement playerMovement;
    private BallPushAttack playerPush;

    private int moveX;
    private int theExtraSpeed;
    private float castPushAction;

    private float tempFloat;

    public override void InitializeAgent()
    {
        rayPer = GetComponent<RayPerception>();
        playerMovement = GetComponent<Movement>();
        playerPush = GetComponent<BallPushAttack>();
    }

    
    public override void CollectObservations()
    {
        float rayDistance = 20f;
        float[] rayAngles = { 15f, 45f, 90f, 135f, 165f, 110f, 70f };
        string[] detectableObjects;

        //TRYING WITH JUST DETECTING THE BALL TO SEE IF THAT WORKS WELL
        detectableObjects = new string[] { "Ball" };

        AddVectorObs(rayPer.Perceive(rayDistance, rayAngles, detectableObjects, 0.5f, 0f));
        
    }

    public override void AgentReset()
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<AITraining>().ResetGame();

        GetComponent<Rigidbody>().velocity = Vector3.zero;
        transform.localPosition = Vector3.zero;

        playerPush.Initialize();
        
    }

    public override void AgentOnDone()
    {
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        //Setting up actions as ints

        //Moving left or right
        tempFloat = Mathf.Clamp(vectorAction[0], -1f, 1f);
        if(tempFloat < 0.2f && tempFloat > -0.2f)
        {
            moveX = 0;
        }
        else
        {
            moveX = (int)Mathf.Sign(tempFloat);
        }

        //increase speed
        tempFloat = Mathf.Clamp(vectorAction[1],0, 1f);
        if (tempFloat < 0.2f)
        {
            theExtraSpeed = 0;
        }
        else
        {
            theExtraSpeed = 1;
        }


        //determines if we should cast pushattack
        tempFloat = Mathf.Clamp(vectorAction[2], 0, 1f);
        if (tempFloat < 0.2f)
        {
            castPushAction = 0;
        }
        else
        {
            castPushAction = 1;
        }


        playerMovement.MovePlayerActions(moveX, theExtraSpeed);

        if (castPushAction == 1) playerPush.PushBall();
    }
}
