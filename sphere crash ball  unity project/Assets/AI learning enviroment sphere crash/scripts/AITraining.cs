﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITraining : MonoBehaviour {

    private BallCornerSpawner[] ballSpawners;

    [SerializeField]
    private float ballIntervalShotTime = 2;
    private float ballIntervalTimer;

    [SerializeField]
    private bool isPlaying = true;


    private Health[] playerHealths;



    // initialization
    void Start()
    {
        ballSpawners = FindObjectsOfType<BallCornerSpawner>();

        playerHealths = FindObjectsOfType<Health>();

        isPlaying = true;

        ballIntervalTimer = ballIntervalShotTime;
    }

    private void FixedUpdate()
    {
        if (isPlaying)
        {
            ballIntervalTimer -= Time.fixedDeltaTime;
            if(ballIntervalTimer < 0)
            {
                ballSpawners[UnityEngine.Random.Range(0, ballSpawners.Length)].SpawnBall();
                ballIntervalTimer = ballIntervalShotTime;
            }
        }
    }



    public void ResetGame()
    {
        foreach (var item in playerHealths)
        {
            item.InitializeHealth();
        }
        var balls = GameObject.FindGameObjectsWithTag("Ball");
        foreach (var ball in balls)
        {
            Destroy(ball);
        }
    }
}
