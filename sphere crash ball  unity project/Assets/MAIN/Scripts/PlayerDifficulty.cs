﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu]
public class PlayerDifficulty : ScriptableObject {

    [SerializeField]
    private DifficultyLevel[] difficulties;
    private TextAsset currentDifficulty;

    public void SelectDifficulty(TextAsset difficulty)
    {
        currentDifficulty = difficulty;
    }

    public TextAsset GetDifficulty()
    {
        if (currentDifficulty == null)
            currentDifficulty = difficulties[1];

        if(difficulties[1].difficultyGraphModel == null)
        {
            Debug.LogError("THE second element of the difficulties does not exist");
        }
        return currentDifficulty;
    }
}
