﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour
{
    public string playerMovementAxis = "Horizontal"; 


    [Space()]
    public float normalSpeed = 1;
    public float fastSpeed = 1.5f;
    public float playerBounds = 2;


    private Vector3 startLocation;
    private Rigidbody rb;


    private float xLocalMovement = 0;

    private float extraSpeed;

    // Use this for initialization
    void Start()
    {
        startLocation = transform.position;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 whereToMove = transform.position + transform.right * normalSpeed* xLocalMovement * Time.fixedDeltaTime * extraSpeed;

        var distanceFromStartLocation = Vector3.Distance(startLocation, whereToMove);

        if(distanceFromStartLocation < playerBounds)
        {
            rb.MovePosition(whereToMove);
        }
        else
        {
            //we are outside the playerbounds we should reign in ourselves.
            var normalizedDirection = ( transform.position - startLocation).normalized;

            rb.MovePosition(startLocation + normalizedDirection * (playerBounds - 0.05f));
        }
    }


    /// <summary>
    /// testing movement with velocity instead of settings the rigidbody position to fix the ludicrous speed you can get from ball bouncings
    /// </summary>
    
    /*void FixedUpdate()
    {

        var distanceFromStartLocation = Vector3.Distance(startLocation, transform.position);

        Debug.Log(distanceFromStartLocation);


        rb.velocity = transform.right * normalSpeed * xLocalMovement * extraSpeed;

        if (distanceFromStartLocation < playerBounds)
        {
            
        }
        else
        {
            //we are outside the playerbounds we should reign in ourselves.
            var normalizedDirection = (transform.position - startLocation).normalized;

            rb.velocity = Vector3.zero;
            rb.MovePosition(startLocation + normalizedDirection * (playerBounds - 0.01f));
        }
    }*/

    public void MovePlayerActions(int xMovement, int increaseSpeed)
    {
        xLocalMovement = xMovement;

        extraSpeed = increaseSpeed == 1 ? fastSpeed : 1.0f;
    }
}
