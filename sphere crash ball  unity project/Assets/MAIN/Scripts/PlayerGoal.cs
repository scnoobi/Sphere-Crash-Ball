﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGoal : MonoBehaviour {


    public Health playerHealth;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))
        {
            playerHealth.Attacked();
            Destroy(other.gameObject);
        }
    }
}
