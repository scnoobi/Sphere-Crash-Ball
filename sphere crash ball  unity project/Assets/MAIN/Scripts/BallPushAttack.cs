﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPushAttack : MonoBehaviour
{

    public float pushForce = 150;
    public LayerMask ballLayer;
    public string pushBallButton = "P1Push Ball";

    public float maxPushes = 2;
    private float currentPushes;
    public float pushRechargeTime = 3;
    private float rechargeTimer;

    private AiRewards playerRewards;

    
    

    [HideInInspector]
    private Vector3 dir;

    private void Start()
    {
        playerRewards = GetComponent<AiRewards>();
        Initialize();
    }

    public void Initialize()
    {
        currentPushes = maxPushes;
        rechargeTimer = pushRechargeTime;
    }

    private void FixedUpdate()
    {
        rechargeTimer -= Time.fixedDeltaTime;
        if(rechargeTimer <0)
        {
            currentPushes++;

            if (currentPushes > maxPushes)
                currentPushes = maxPushes;

            rechargeTimer = pushRechargeTime;
        }
    }

    public void PushBall()
    {
        if (currentPushes > 0)
        {
            currentPushes--;

            Collider[] collidersHit = Physics.OverlapSphere(transform.position + transform.forward * -1 * 0.4f, 1.6f, ballLayer);

            if(playerRewards)
            {
                if (collidersHit.Length > 0)
                    playerRewards.AddAAgentReward(collidersHit.Length);
            }
            

            //in a coroutine because we only want to push 1 ball per frame to reduce lag.
            StartCoroutine(BallPushing(collidersHit));
        }
    }

    private IEnumerator BallPushing(Collider[] colliders)
    {
        for (int i = 0; i < colliders.Length; i++)
        {

            var colliderRigidbody = colliders[i].attachedRigidbody;
            RaycastHit hit;
            
            if (colliderRigidbody)
            {                
                dir = (transform.position - colliders[i].transform.position) * 5;
                bool hitSomething = Physics.Raycast(colliders[i].transform.position, dir ,out hit);


                if (hitSomething)
                {
                    colliderRigidbody.AddForce(hit.normal.normalized * pushForce, ForceMode.Impulse);                    
                }
            }

            yield return new WaitForFixedUpdate();
        }
    }
}
