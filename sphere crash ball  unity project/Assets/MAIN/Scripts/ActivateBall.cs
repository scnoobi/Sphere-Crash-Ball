﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBall : MonoBehaviour
{


    private float currentSpeed;
    public float pushSpeedAtStart = 100;
    
    public float groundLevel = 0.5f;

    public SphereCollider triggerSphere;
    public SphereCollider collisionSphere;
    private Rigidbody rb;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        EnablePlay(false);
        rb.AddForce(transform.forward * pushSpeedAtStart, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        currentSpeed = rb.velocity.sqrMagnitude;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Floor"))
        {
            EnablePlay();
        }
    }

    private void EnablePlay(bool state = true)
    {
        collisionSphere.enabled = state;
        triggerSphere.enabled = !state;

        if (state)
        {
            Vector3 tempPosition = transform.position;
            tempPosition.y = groundLevel;

            transform.position = tempPosition;

            rb.constraints = RigidbodyConstraints.FreezePositionY;
        }
            
        else
            rb.constraints = RigidbodyConstraints.None;
    }
}
