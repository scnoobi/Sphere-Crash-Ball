﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCornerSpawner : MonoBehaviour
{
    public float randomXRange;
    public Transform ballHolder;

    private Quaternion startDirection;

    [Space()]
    public GameObject ball;
    public float pushForce;

    // Use this for initialization
    void Start()
    {
        startDirection = Quaternion.LookRotation( transform.forward);
    }


    

    public void SpawnBall()
    {
        float randomXDegrees = Random.Range(-randomXRange, randomXRange);
        Quaternion newRotation = startDirection;
        newRotation *= Quaternion.Euler(0, randomXDegrees, 0);
        
        Instantiate(ball, transform.position, newRotation,ballHolder);
    }
}
