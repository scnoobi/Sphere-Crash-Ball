﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [Header("Ai Difficulties")]
    public Brain easyDifficulty;
    public Brain normalDifficulty;
    public Brain hardDifficulty;
    public Brain ImpossibleDifficulty;




    [Header("Gamemode settings")]
    [SerializeField]
    private float ballIntervalShotTime = 2;
    [SerializeField]
    private float gameStartTime = 3;
    [SerializeField]
    private int amountOfRoundToWinTheGame = 2;


    private BallCornerSpawner[] ballSpawners;

    private bool isPlaying = false;

    //Gets initialized by playerHealths
    private int[] playerRoundScore;


    private Health[] playerHealths;
    private int amountOfPlayersAlive = 0;



    // initialization
    void Start()
    {
        ballSpawners = FindObjectsOfType<BallCornerSpawner>();

        playerHealths = FindObjectsOfType<Health>();
        playerRoundScore = new int[playerHealths.Length];

        var brainToUse =  CheckWhichBrainWeShouldUse();

        var aiAgents = FindObjectsOfType<CrashBallAgent>();
        foreach (var agent in aiAgents)
        {
            agent.GiveBrain(brainToUse);
        }

        StartCoroutine(GameLoop());
    }

    Brain CheckWhichBrainWeShouldUse()
    {
        var allBrains = FindObjectOfType<AiDifficultyLevel>
        return hardDifficulty;
    }




    private IEnumerator GameLoop()
    {

        StartCoroutine(ExecuteBallShooting());

        //wait for x seconds until we start the game
        yield return StartCoroutine(Timer(gameStartTime));
        StartGame();

    }
    private void StartGame()
    {
        isPlaying = true;

        //TODO check this code. if we need it
        StartCoroutine(DoWeHaveAGameWinner());
    }

    private IEnumerator DoWeHaveAGameWinner()
    {
        while (isPlaying)
        {
            amountOfPlayersAlive = 0;
            for (int i = 0; i < playerHealths.Length; i++)
            {
                if (playerHealths[i].currentHealth > 0)
                {
                    amountOfPlayersAlive++;
                }
            }
            if (amountOfPlayersAlive <= 1)
            {
                
                isPlaying = false;
                RoundIsOver();
            }
            yield return new WaitForSeconds(0.1f);
        }

    }

    private void RoundIsOver()
    {
        //TODO make a method that checks a player won the game. you should check if it is a draw or a pure win!


        //check who is alive
        for (int i = 0; i < playerHealths.Length; i++)
        {
            if (playerHealths[i].currentHealth > 0)
            {
                playerRoundScore[i]++;

                if(playerRoundScore[i] >= amountOfRoundToWinTheGame)
                {
                    GameIsFinished();
                }
                else
                {
                    StartANewRound();
                }
                break;
            }
        }
    }

    private void StartANewRound()
    {



        //Player a victory cheer for the winning player.
        //then start a new round after 3 seconds or after the victory cheer.




        ///Starting new round

        //Refresh the gameboard: Initialize Health, Remove all balls in the game.
        GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        for (int i = 0; i < balls.Length; i++)
        {
            Destroy(balls[i]);
        }

        for (int i = 0; i < playerHealths.Length; i++)
        {
            playerHealths[i].InitializeHealth();
        }

        StopAllCoroutines();
        StartCoroutine(GameLoop());
    }

    private void GameIsFinished()
    {



        //Player a victory cheer for the winning player.
        //then load the main menu after 3 seconds or after the victory cheer
        

        SceneManager.LoadScene(0);

    }

    private IEnumerator ExecuteBallShooting()
    {
        while (true)
        {
            yield return new WaitForSeconds(ballIntervalShotTime);
            if (isPlaying)
                ballSpawners[UnityEngine.Random.Range(0, ballSpawners.Length)].SpawnBall();
        }
    }

    private IEnumerator Timer(float time)
    {
        float timer = 0;
        if (timer < time)
        {
            timer += Time.deltaTime;
        }
        yield return null;
    }
}