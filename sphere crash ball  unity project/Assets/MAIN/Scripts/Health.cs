﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int maxHealth = 15;
    public int currentHealth { private set; get; }
    public bool aiTraining = false;

    public AiRewards agentRewards;
    public GameObject barrier;
    

    private void Awake()
    {
        InitializeHealth();
    }

    public void InitializeHealth()
    {
        
        currentHealth = maxHealth;
        gameObject.SetActive(true);
        barrier.SetActive(false);
    }

    public void Attacked()
    {
        //to make it so this can't get called twice
        if (currentHealth <= 0)
            return;

        currentHealth--;

        if(agentRewards)
            agentRewards.AddAAgentReward(-8f);

        if(currentHealth <= 0)
        {
            barrier.SetActive(true);
            agentRewards.Finished();
            if(aiTraining == false)
                gameObject.SetActive(false); 
        }
    }
}
